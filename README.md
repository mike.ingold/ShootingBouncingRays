# Shooting Bouncing Rays Test

Running `main.jl` will process all ray-trace models in this test.

Multithreading is enabled by default but can be disabled by setting
```RayTraceEngine.ENABLE_THREADING = false```

## Examples

![Plane Wave](assets/problem3b.png "Plane Wave")

The figure above repesents a plane wave scattering off of a concave reflector.

![Radial Wave](assets/problem3c.png "Radial Wave")

The figure above repesents a reciprocal point source scattering off of the
same concave reflector.
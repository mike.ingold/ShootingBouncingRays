#===============================================================================
                        DATA STRUCTURES
===============================================================================#

"""
    LineSegment represents a line from Point `a` to Point `b` with a unit vector
from Point `a` to Point `b` represented by `orientation`.
"""
struct LineSegment{T<:Real}
    a::SVector{2,T}
    b::SVector{2,T}
    orientation::SVector{2,T}
end

"""
    GeometrySet
"""
mutable struct GeometrySet{T<:Real}
    _source::Vector{String}
    lines::Vector{LineSegment{T}}
end

"""
    GeometrySet{T}(fname)
Construct a GeometrySet based on the data located in a `CSV file`.
"""
function GeometrySet{T}(fname::String)::GeometrySet{T} where {T<:Real}
    # Read data into a DataFrame; first row is a header
    @debug "Reading in data from $(fname)"
    data = CSV.read(fname, DataFrame, header=1)

    # Iteratively ingest line segments from CSV by row
    #   Columns 1-2 represent first point (x,y)
    #   Columns 3-4 represent second point (x,y)
    line_set = Vector{LineSegment{T}}();
    for i in 1:nrow(data)
        a = SVector{2,T}(data[i,1], data[i,2]);
        b = SVector{2,T}(data[i,3], data[i,4]);
        dr = b - a;
        orientation = dr/abs(dr);
        this_line = LineSegment{T}(a,b,orientation);
        append!(line_set, [this_line])
    end

    return GeometrySet{T}([fname], line_set)
end

#===============================================================================
                METHODS OVERLOADED FROM BASE
===============================================================================#

import Base.abs
import Base.show

Base.abs(this::SVector{2,T}) where {T<:Real} = hypot(this[1], this[2])

function Base.show(io::IO, ls::LineSegment)
    print(io, "LineSegment from $(ls.a) to $(ls.b)",
              " with unit vector $(ls.orientation)")
end

function Base.show(io::IO, gs::GeometrySet)
    n = length(gs.lines);
    print(io, "GeometrySet with $(n) lines from source(s): $(gs._source)")
end

#===============================================================================
                        METHODS
===============================================================================#

"""
    join_geometry(g1, g2)
Combine two GeometrySet's by appending lines from the `second` to the `first`.
"""
function join_geometry(g1::GeometrySet, g2::GeometrySet)
    new_g = deepcopy(g1);
    append!(new_g._source, g2._source);
    append!(new_g.lines, g2.lines);
    return new_g
end

#===============================================================================
                        METHODS - RAY SOURCES
===============================================================================#

"""
    get_point_source(loc; arange, n)
Generate a RayTraceSet for a point source at a `location`. Optionally constrain
    the `range of angles` and specify a `number of rays` within that angle.
"""
function get_point_source(loc::Vector{T}; arange::Vector{T}=[0.0,360.0],
        n::Int=100) where {T<:Real}
    # Get position and emanating unit orientation vectors
    position = SVector{2}(loc);
    theta = range(arange[1],arange[2],n+1)[1:end-1]
    unit_vectors = SVector{2}.(cosd.(theta), sind.(theta))

    # Load sources -> RayStates -> RayTraces
    rays = map(x -> RayState{T}(position, x), unit_vectors);
    raytraces = map(x -> RayTrace{T}(x), rays);

    # Load all RayTraces into a single RayTraceSet
    return RayTraceSet{T}(["get_plane_source"], raytraces)
end

"""
    get_plane_source(location, heading; width, n)
Generate a RayTraceSet of a plane wave source at a `location` emitting in the 
    direction of a `heading` in degrees. Optionally specify the wave `width`
    (defaults to unit width) and `number of rays` (defaults to 10).
"""
function get_plane_source(location::Vector{T}, heading::T; width::T=1,
        n::Int=10) where {T<:Real}
    # Get origin position and orientation of plane wave propagation
    origin = SVector{2,T}(location);
    orientation = SVector{2,T}(cosd(heading), sind(heading));

    # Determine n equal-spaced source positions along aperture width
    orthogonal_axis = SVector{2,T}(sind(heading), -cosd(heading));
    span = range(-width/2, width/2, n);
    sources = map(x -> x * orthogonal_axis, span);
    sources = map(x -> x + origin, sources);

    # Load sources -> RayStates -> RayTraces
    rays = map(x -> RayState{T}(x, orientation), sources);
    raytraces = map(x -> RayTrace{T}(x), rays);

    # Load all RayTraces into a single RayTraceSet
    return RayTraceSet{T}(["get_plane_source"], raytraces)
end

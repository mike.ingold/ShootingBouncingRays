module RayTraceEngine
    using CSV
    using DataFrames
    using LinearAlgebra
    using Logging
    using Plots
    using StaticArrays

    include("raynames.jl")

    include("geometry.jl")
    export GeometrySet, LineSegment
    export join_geometry

    include("rays.jl")
    export RayState, RayTrace, RayTraceSet
    export iscomplete, process!

    include("ray_sources.jl")
    export get_plane_source, get_point_source

    include("visualization.jl")
    export plot_geometry, plot_model

    _ENABLE_THREADING = true;
end
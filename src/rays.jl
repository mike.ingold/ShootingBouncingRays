const MIN_RAY_DIST = 0.01;

#===============================================================================
                        DATA STRUCTURES
===============================================================================#

struct RayState{T<:Real}
    position::SVector{2,T}
    orientation::SVector{2,T}
end

mutable struct RayTrace{T<:Real}
    _name::String
    trajectory::Vector{RayState{T}}
    terminated::Bool
end

function RayTrace{T}() where {T<:Real}
    return RayTrace{T}(get_name(),  Vector{RayState{T}}(), false)
end

function RayTrace{T}(ray::RayState{T}) where {T<:Real}
    return RayTrace{T}(get_name(), [ray], false)
end

mutable struct RayTraceSet{T<:Real}
    _source::Vector{String}
    rays::Vector{RayTrace{T}}
end

function RayTraceSet{T}() where {T<:Real}
    return (Vector{String}(), Vector{RayTrace{T}}())
end

function RayTraceSet{T}(fname::String) where {T<:Real}
    # Read data into a DataFrame; first row is a header
    @debug "Reading in data from $(fname)"
    data = CSV.read(fname, DataFrame, header=1)

    # Iteratively ingest line segments from CSV by row
    #   Columns 1-2 represent ray origin (x,y)
    #   Columns 3-4 represent ray orientation (x,y)
    #   Column 5 represents amplitude
    raystates = Vector{RayState{T}}();
    for i in 1:nrow(data)
        position = SVector{2,T}(data[i,1], data[i,2]);
        orientation = SVector{2,T}(data[i,3], data[i,4]);
        orientation = orientation/abs(orientation);

        this_raystate = RayState{T}(position, orientation);
        append!(raystates, [this_raystate])
    end
    raytraces = RayTrace{T}.(raystates);

    return RayTraceSet{T}([fname], raytraces)
end

#===============================================================================
                        METHODS - UTILITIES
===============================================================================#

import Base.show

function Base.show(io::IO, ray::RayState)
    print(io, "$(typeof(ray)) with position $(ray.position)",
              " and orientation $(ray.orientation)")
end

function Base.show(io::IO, trace::RayTrace)
    status = trace.terminated ? "termianted" : "active";
    n = length(trace.trajectory);
    print(io, "$(typeof(trace)) named $(ray._name),",
              "$(status) with $(n) trajectory states");
end

function Base.show(io::IO, set::RayTraceSet)
    n = length(set.rays);
    print(io, "$(typeof(set)) from source $(set._source) with $(n) rays");
end

"""
    iscomplete(rayset)
Determine if every RayTrace in a given `RayTraceSet` has terminated.
"""
function iscomplete(rayset::RayTraceSet)
    # Vector of rays -> vector of their `terminated` properties
    termination_states = getproperty.(rayset.rays, :terminated)
    # Return true IFF all `terminated` values == true
    return all(termination_states)
end

"""
    isbetween(c, a, b)
Determine if point `c` is on or inside a rectangle bounded by points `a` and `b`.
"""
function isbetween(c::SVector{2,T}, a::SVector{2,T}, b::SVector{2,T}
        ) where {T<:Real}
    xmin = min(a[1], b[1]);
    xmax = max(a[1], b[1]);
    ymin = min(a[2], b[2]);
    ymax = max(a[2], b[2]);
    return ( ( xmin <= c[1] <= xmax ) && ( ymin <= c[2] <= ymax ) )
end

"""
    distance(ray, a)
Return the distance from a `ray`s origin to a `point`.
"""
function distance(ray::RayState{T}, a::SVector{2,T})::T where {T<:Real}
    return abs(ray.position - a)
end

"""
    does_intersect(ray, line)
Determine whether a `ray` intersects with a `line`.
"""
function does_intersect(ray::RayState{T}, line::LineSegment{T}) where {T<:Real}
    # Set a reference point marginally in front of the actual ray
    # Get position of points A and B relative to this reference point
    # Prevents false alarms from quantization error when exiting a surface
    reference_pos = ray.position + (MIN_RAY_DIST * ray.orientation);
    a = line.a - reference_pos;
    b = line.b - reference_pos;

    # Get headings of ray and to points A and B
    heading_of_ray = atan(ray.orientation[2], ray.orientation[1]);
    heading_to_a = atan(a[2], a[1]);
    heading_to_b = atan(b[2], b[1]);
    goalposts = sort([heading_to_a, heading_to_b]);

    # Check if ray heading goes between points A and B
    # atan(y,x) outputs on range [-pi, pi]
    # If the angle(A,B) < pi, then the net is in the interior angle
    # If the angle(A,B) > pi, then the net is in the exterior angle
    if (goalposts[2]-goalposts[1]) < pi
        return ( goalposts[1] < heading_of_ray < goalposts[2] )
    else
        return !( goalposts[1] < heading_of_ray < goalposts[2] )
    end
end

#===============================================================================
                        METHODS - PATH GENERATION
===============================================================================#

"""
    get_reflection_orientation(incident, tangent)
For an `incident vector` on a `surface tangent`, calculate the reflection.
"""
function get_reflection_orientation(incident::SVector{2,T}, tangent::SVector{2,T}
        ) where {T<:Real}
    # Get the surface normal vector on the incident side of surface
    normal = [-tangent[2], tangent[1]];
    if dot(incident, normal) > 0
        normal = -normal;
    end

    # Using the tangent and normal as basis vectors, get reflected vector
    dt = dot(incident, tangent);
    dn = -dot(incident, normal);
    ref = (dt .* tangent) + (dn .* normal);
    ref = ref./abs(ref);
    return SVector{2,T}(ref)
end

"""
    find_reflection(ray, line)
Find the reflected ray generated by a `ray` incident upon a `line`.
"""
function find_reflection(ray::RayState{T}, line::LineSegment{T}) where {T<:Real}
    # Alias the ray values
    ray_u = ray.orientation[1];
    ray_v = ray.orientation[2];
    ray_x0 = ray.position[1];
    ray_y0 = ray.position[2];

    # Alias the geometry values
    geo_u = line.orientation[1];
    geo_v = line.orientation[2];
    geo_x0 = line.a[1];
    geo_y0 = line.a[2];

    # Use Cramer's Rule to solve for intersection
    A = SMatrix{2,2}(ray.orientation..., -line.orientation...);
    b = SVector{2}(line.a - ray.position);
    s = inv(A) * b;
    sr = s[1];
    intercept = ray.position + (sr * ray.orientation)
    
    orientation = get_reflection_orientation(ray.orientation, line.orientation);
    return RayState(intercept, orientation)
end

"""
    march!(raytrace, world)
Advance a `ray trace` to the next collision in a `world`.
"""
function march!(raytrace::RayTrace{T}, world::GeometrySet{T}) where {T<:Real}
    # If ray is already marked as terminated, just bail out
    if raytrace.terminated
        return
    end

    # Get latest state in trajectory
    ray = raytrace.trajectory[end];

    # Find all lines that intersect with the ray
    # If no intersections are possible, mark this ray as terminated and bail out
    candidates = filter(x -> does_intersect(ray,x), world.lines);
    if isempty(candidates)
        raytrace.terminated = true;
        return
    end

    # Calculate all reflections, select the nearest one
    # TODO handle literal corner case where two have same distance
    reflection_states = map(x->find_reflection(ray,x), candidates);
    sort!(reflection_states, by=(x->distance(ray, x.position)));
    push!(raytrace.trajectory, reflection_states[1]);

    # If debugging is enabled, log internal decision data
    @debug  ray  candidates  reflection_states
end

"""
    process(raytrace, world; n)
Advance a `ray trace` through a `world` until it escapes. Optionally specify a 
    `maximum number of steps` (default Inf).
"""
function process!(raytrace::RayTrace{T}, world::GeometrySet{T}; n=Inf
        ) where {T<:Real}
    i = 0;
    while !raytrace.terminated && (i<n)
        @debug "Marching ray $(raytrace._name) on step $(i+1)"
        march!(raytrace, world)
        i = i + 1;
    end
end

"""
    process(raytraceset, world; n)
Advance an entire `set of ray traces` through a `world`. Optionally specify a 
    `maximum number of steps` for each trace (default Inf).
"""
function process!(raytraceset::RayTraceSet{T}, world::GeometrySet{T}; n=Inf
        ) where {T<:Real}
    # Process individual raytraces in parallel
    if _ENABLE_THREADING
        Threads.@threads for raytrace in raytraceset.rays
            process!(raytrace, world, n=n);
        end
    else
        for raytrace in raytraceset.rays
            process!(raytrace, world, n=n);
        end
    end
end

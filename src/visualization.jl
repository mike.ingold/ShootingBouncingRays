const FF_SIZE = 1000.0;

"""
    plot_geometry(geometry, fname; title, xlims, ylims)
Plot a `GeometrySet`, saving it with a given `filename`.
"""
function plot_geometry(geometry::GeometrySet, fig_fname::String;
    xlims=:auto, ylims=:auto, title="Geometry"
)
    # Make an empty plot canvas
    plot(
        label=missing,
        title=title,
        xlabel="X Coordinate",
        ylabel="Y Coordinate",
        xlims=xlims,
        ylims=ylims,
        aspect_ratio=:equal,
        #dpi=200,
        size=(800,600)
    )

    # Draw the geometry on the plot
    for line in geometry.lines
        ax = line.a[1];
        ay = line.a[2];
        bx = line.b[1];
        by = line.b[2];
        ux = ax + line.orientation[1];
        uy = ay + line.orientation[2];

        # Draw lines from a->b
        plot!(
            [ax, bx], [ay, by],
            label=missing,
            linewidth=3,
            linecolor=:black
        )
        
        # Draw orientation vector originating from a
        plot!(
            [ax, ux], [ay, uy],
            label=missing,
            linewidth=3,
            linecolor=:red,
            linestyle=:dot,
            arrow=true
        )
    end

    # Save plot to file and echo filename for confirmation
    savefig(fig_fname)
    @info "Saved figure to file $(fig_fname)"
end

function plot_model(geometry::GeometrySet{T}, rayset::RayTraceSet{T},
    fname::String; xlims=:auto, ylims=:auto, title="Ray Trace Model"
        ) where {T<:Real}
    # Make an empty plot canvas
    plot(
        label=missing,
        title=title,
        xlabel="X Coordinate",
        ylabel="Y Coordinate",
        xlims=xlims,
        ylims=ylims,
        aspect_ratio=:equal,
        #dpi=200,
        size=(800,600)
    )

    # Draw the geometry on the plot
    for line in geometry.lines
        ax = line.a[1];
        ay = line.a[2];
        bx = line.b[1];
        by = line.b[2];
        ux = ax + line.orientation[1];
        uy = ay + line.orientation[2];

        # Draw lines from a->b
        plot!(
            [ax, bx], [ay, by],
            label=missing,
            linewidth=3,
            linecolor=:black
        )
    end

    # Plot all the ray traces
    for raytrace in rayset.rays
        # Build a pair of parametric x and y vectors from this trace's trajectory
        ray_path = getproperty.(raytrace.trajectory, :position);
        ray_path_x = getindex.(ray_path, 1);
        ray_path_y = getindex.(ray_path, 2);

        # Extend the final position/orientation into the far-field
        route_to_ff = FF_SIZE * raytrace.trajectory[end].orientation;
        ray_path_x = [ray_path_x; route_to_ff[1]];
        ray_path_y = [ray_path_y; route_to_ff[2]];

        # Plot this traced ray
        plot!(ray_path_x, ray_path_y,
            label=missing,
            linewidth=1.5,
            linestyle=:dash,
            linecolor=:auto,
            markershape=:cross
        )
    end

    # Save plot to file and echo filename for confirmation
    savefig(fname)
    @info "Saved figure to file $(fname)"
end

#===============================================================================
                           PROLOGUE
===============================================================================#

# Define file structure
DIR_ROOT  = @__DIR__;
DIR_SRC   = joinpath(DIR_ROOT, "src");
DIR_ASSET = joinpath(DIR_ROOT, "assets");
DIR_FIGS  = joinpath(DIR_ROOT, "figures");

# Enable debug mode
#ENV["JULIA_DEBUG"] = all;

# Ensure src directory is executable and then load modules
if !( DIR_SRC in LOAD_PATH ) push!(LOAD_PATH, DIR_SRC); end

using Logging
using RayTraceEngine

#===============================================================================
                        LOCATE THE DATA
===============================================================================#

# Input files -- target geometries
const ASSET_FNAME_GEOMETRY       = joinpath(DIR_ASSET, "geometry.csv");
const ASSET_FNAME_COARSE_SPHERE  = joinpath(DIR_ASSET, "coarse_sphere.csv");
const ASSET_FNAME_TILTED_CORNER  = joinpath(DIR_ASSET, "tilted_corner.csv");
const ASSET_FNAME_CONCAVE        = joinpath(DIR_ASSET, "concave.csv");

# Input file - source rays
const ASSET_FNAME_UNIFORM_SOURCE = joinpath(DIR_ASSET, "uniform_source.csv");

# Figure naming convention
fig_fname(number, letter) = joinpath(DIR_FIGS, "problem$(number)$(letter).png");

# Ingest rays
rayset = RayTraceSet{Float64}(ASSET_FNAME_UNIFORM_SOURCE);

#===============================================================================
                               PROBLEM 1
===============================================================================#

@info "Problem 1 - Double Reflectors"
p1_geometry = GeometrySet{Float64}(ASSET_FNAME_GEOMETRY);

# Run given rayset against model and plot results
p1a_rayset = deepcopy(rayset);
process!(p1a_rayset, p1_geometry);
plot_model(p1_geometry, p1a_rayset, fig_fname(1,"a"),
    title="Double Reflector", xlims=(-2, 6), ylims=(-2, 6) )

# Run a denser plane wave against this model and plot results
p1b_rayset = get_plane_source([-10.0,0.0], 0.0, width=2.5, n=30);
process!(p1b_rayset, p1_geometry);
plot_model(p1_geometry, p1b_rayset, fig_fname(1,"b"),
    title="Double Reflector", xlims=(-2, 6), ylims=(-2, 6) )

#===============================================================================
                        PROBLEM 2 (COARSE SPHERE)
===============================================================================#

@info "Problem 2 - Coarse Sphere"

# Run given rayset against model and plot results
p2a1_rayset = deepcopy(rayset);
p2a_geometry = GeometrySet{Float64}(ASSET_FNAME_COARSE_SPHERE);
process!(p2a1_rayset, p2a_geometry);
plot_model(p2a_geometry, p2a1_rayset, fig_fname(2,"a1"),
    title="Coarse Sphere", xlims=(-2, 4), ylims=(-4, 4))

# Run improved plane source against model and plot results
p2a2_rayset = get_plane_source([-10.0,0.0], 0.0, width=2.5, n=30);
process!(p2a2_rayset, p2a_geometry);
plot_model(p2a_geometry, p2a2_rayset, fig_fname(2,"a2"),
    title="Coarse Sphere", xlims=(-2, 4), ylims=(-4, 4))

#===============================================================================
                        PROBLEM 2 (TILTED CORNER)
===============================================================================#

@info "Problem 2 - Tilted Corner"

# Run given rayset against model and plot results
p2b1_rayset = deepcopy(rayset);
p2b_geometry = GeometrySet{Float64}(ASSET_FNAME_TILTED_CORNER);
process!(p2b1_rayset, p2b_geometry);
plot_model(p2b_geometry, p2b1_rayset, fig_fname(2,"b1"),
    title="Tilted Corner", xlims=(-2, 3), ylims=(-4, 2))

# Run improved plane source against model and plot results
p2b2_rayset = get_plane_source([-10.0,0.0], 0.0, width=2.5, n=19);
process!(p2b2_rayset, p2b_geometry);
plot_model(p2b_geometry, p2b2_rayset, fig_fname(2,"b2"),
    title="Tilted Corner", xlims=(-2, 3), ylims=(-4, 2))

#===============================================================================
                        PROBLEM 2 (COMBINED)
===============================================================================#

@info "Problem 2 - Combined"

# Run given rayset against model and plot results
p2c1_rayset = deepcopy(rayset);
p2c_geometry = join_geometry(p2a_geometry, p2b_geometry);
process!(p2c1_rayset, p2c_geometry);
plot_model(p2c_geometry, p2c1_rayset, fig_fname(2,"c1"),
    title="Tilted Corner with Sphere", xlims=(-2, 4), ylims=(-4, 4))

# Run improved plane source against model and plot results
p2c2_rayset = get_plane_source([-10.0,0.0], 0.0, width=2.5, n=30);
process!(p2c2_rayset, p2c_geometry);
plot_model(p2c_geometry, p2c2_rayset, fig_fname(2,"c2"),
    title="Tilted Corner with Sphere", xlims=(-2, 4), ylims=(-4, 4))

#===============================================================================
                           PROBLEM 3
===============================================================================#

@info "Problem 3 - Concave Reflector"

# Run given rayset against model and plot results
p3a_rayset = deepcopy(rayset);
p3_geometry = GeometrySet{Float64}(ASSET_FNAME_CONCAVE);
process!(p3a_rayset, p3_geometry);
plot_model(p3_geometry, p3a_rayset, fig_fname(3,"a"),
    title="Concave Reflector", xlims=(-3, 3), ylims=(-2, 6))

# Run improved plane source against model and plot results
p3b_rayset = get_plane_source([-10.0,0.0], 0.0, width=2.5, n=30);
process!(p3b_rayset, p3_geometry);
plot_model(p3_geometry, p3b_rayset, fig_fname(3,"b"),
    title="Concave Reflector", xlims=(-3, 3), ylims=(-2, 6))

# Run point source at caustic against model and plot results
p3c_rayset = get_point_source([-1.5,4.0]);
process!(p3c_rayset, p3_geometry);
plot_model(p3_geometry, p3c_rayset, fig_fname(3,"c"),
    title="Concave Reflector", xlims=(-3, 3), ylims=(-2, 6))
